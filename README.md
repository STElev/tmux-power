# Tmux Powerline Theme

Yet another powerline theme for tmux. Modified from [tmux-power](https://github.com/wfxr/tmux-power) 

###  Installation

**Install with Tmux plugin manager**

```tmux
set -g @plugin 'https://gitlab.com/STElev/tmux-power.git'
```

**Install manually**

Clone the repo somewhere and source it in `.tmux.conf`:

```tmux
git clone https://gitlab.com/STElev/tmux-power.git ~/.tmux/plugins
run-shell "/path/to/tmux-power.tmux"
```

*NOTE: Options should be set before sourcing.*

###  Themes

**Default**: `set -g @tmux_power_theme 'default'`
Set this theme if you want to honor the terminal colorscheme. To be used with
something like [pywal](https://github.com/dylanaraps/pywal) for instance.

### ⚙  Customizing

You can define your favourite main color if you don't like any of above.

```tmux
set -g @tmux_power_theme '#483D8B' # dark slate blue
```

You can change the date and time formats using strftime:

```tmux
set -g @tmux_power_date_format '%F'
set -g @tmux_power_time_format '%T'
```

You can also customize the icons:

```tmux
set -g @tmux_power_date_icon ' ' # set it to a blank will disable the icon
set -g @tmux_power_time_icon '🕘' # emoji can be used if your terminal supports
set -g @tmux_power_user_icon 'U'
set -g @tmux_power_session_icon 'S'
set -g @tmux_power_upload_speed_icon '↑'
set -g @tmux_power_download_speed_icon '↓'
set -g @tmux_power_left_arrow_icon '<'
set -g @tmux_power_right_arrow_icon '>'
```
*The default icons use glyphs from [nerd-fonts](https://github.com/ryanoasis/nerd-fonts).*
